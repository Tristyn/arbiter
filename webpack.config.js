var webpack = require("webpack")
var path = require("path");
module.exports = {  
	entry: './src/app/Main.ts',
	devtool: "source-map",
	cache: true,

	plugins: [ 
		//new webpack.SourceMapDevToolPlugin({
		//	test: [/\.ts$/],
		//	filename: "bin/static/app.js.map"
		//}),
		//new UglifyJSPlugin({ sourceMap: true }) 
	],

	output: {
	  filename: 'bin/arbiter.js'
	},

	resolve: {
		modules: ["node_modules"],
	  extensions: ['.webpack.js', '.web.js', '.ts', '.js']
	},

	module: {
	  rules: [
			{ 
				test: /\.ts$/, 
				loaders: ['ts-loader']
			},
			{
        test: /\.json$/,
				loader: 'json-loader'
      }
	  ]
	},
	
	node: {
		// Fixes can't resolve 'fs' module
		// https://github.com/webpack-contrib/css-loader/issues/447
		fs: "empty"
 	}
}