import { SimpleFXServiceFactory } from "gdax-trading-toolkit/build/src/factories";
import * as GTT from "gdax-trading-toolkit";
import { DummyFXRateCalculator } from "./DummyFxRateCalculator";
import * as BigNumber from "bignumber.js";
import { FXService } from "gdax-trading-toolkit/build/src/FXService";

const logger = GTT.utils.ConsoleLoggerFactory();
const oneHour = 1000 * 60 * 60 + 1;

export function UsdCadFXService(dummyMode: boolean): FXService {
	if (dummyMode == false || dummyMode == undefined) {
		return SimpleFXServiceFactory("openexchangerates", logger, oneHour)
			.addCurrencyPair({from: "USD", to: "CAD"});
	} else {
		var dummyRate = new BigNumber(process.env.DUMMYUSDCADRATE);
		var calculator = new DummyFXRateCalculator();
		calculator.addPair({from: "USD", to: "CAD"}, dummyRate)
		return new FXService({
			logger,
			calculator,
			activePairs: [{from: "USD", to: "CAD"}]
		});
	}
}