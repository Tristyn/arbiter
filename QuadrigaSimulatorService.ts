import * as QuadrigaCX from "quadrigacx";
import { IQuadrigaService, QuadrigaService, QuadrigaBalance, QuadrigaOrderbook, QuadrigaOpenOrder } from "./QuadrigaService";
const apiDefaults = {
	book: "eth_cad"
}

const urls = {
	orderBook: "order_book",
	balance: "balance",
	userTransactions: "user_transactions",
	buy: "buy",
	sell: "sell",
	transactions: "transactions"
}

export class QuadrigaSimulatorService implements IQuadrigaService {

	dataSource: IQuadrigaService;
	api: QuadrigaCX;
	_balance: QuadrigaBalance;
	//functions
	//api(method, params, cb)
	//public_request(path, params, cb)
	//private_request(path, params, cb)

	constructor(dataSource: IQuadrigaService) {
		this.dataSource = dataSource;
		this.api = new QuadrigaCX(process.env.QUADRIGA_USERID,
			process.env.QUADRIGA_KEY, process.env.QUADRIGA_SECRET);
	}

	async initialize() {
		this._balance = await this.dataSource.balance();
	}

	private stringsToFloats(obj) : any {
		for (var property in obj) {
			if (obj.hasOwnProperty(property)) {
				if (typeof obj[property] == "object") {
					obj[property] = this.stringsToFloats(obj[property]);
				}
				else if (typeof obj[property] == "string"){
					var float = parseFloat(obj[property]);
					if (!isNaN(float)) {
						obj[property] = float;
					}
				}
			}
		}
	}

	transactions() {
		return this.dataSource.transactions();
	}

	async orderBook(): Promise<QuadrigaOrderbook> {
		return this.dataSource.orderBook();
		//book.asks.map(val => { return {amount: Number.parseFloat(<any>val.amount)})
	}
	
	balance(): Promise<QuadrigaBalance> {
		return Promise.resolve(JSON.parse(JSON.stringify(this._balance)));
	}

	buy(amount: number, price: number, nonce: number) {
		var total = amount * price;
		console.assert(this._balance.cad_balance >= total);
		this._balance.cad_balance -= total;
		this._balance.cad_available -= total;
		this._balance.eth_balance += amount;
		this._balance.eth_available += amount;
		return <Promise<QuadrigaOpenOrder>>Promise.resolve({ amount, price, id: "-1", status: 1, type: 0 });
	}

	sell(amount: number, price: number, nonce: number) {
		var total = amount * price;
		console.assert(this._balance.eth_balance >= amount);
		this._balance.cad_balance += total;
		this._balance.cad_available += total;
		this._balance.eth_balance -= amount;
		this._balance.eth_available -= amount;
		return <Promise<QuadrigaOpenOrder>>Promise.resolve({ amount, price, id: "-1", status: 1, type: 1 });
	}

	cancel(id) {
		//orders fulfill immediately in the sim
		return Promise.resolve(false);
	}

	openOrders() {
		return Promise.resolve([]);
	}

	userTransactions() {
		return this.dataSource.userTransactions();
	}
	
}