import { EventEmitter } from "events";
import { GdaxTicker } from "./GdaxTicker";
import * as Queue from "queue";
import { TickerMessage } from "gdax-trading-toolkit/build/src/core";
import { QuadrigaBalance, QuadrigaOrderbook, QuadrigaTransaction, QuadrigaOpenOrder, IQuadrigaService, QuadrigaUserTransaction } from "./QuadrigaService";

/*

When gdax > buyPrice * fee * safeMargin then
sell limit min: at gdax price * price adjustment & volume adjustment
sell limit max: top of the book with volume adjustment

when holding cad
buy limit max: sell price / fee
buy limit min: top of the book with volume adjustment
stop loss buy limit: top of the book when gdax > buy orders * 1.01 & volume adjustment
stop loss buy back trigger: when gdax > our sell price * 1.03 for 3 seconds, and quadriga buy price > our sell price * 1.01

easy stop loss buy back strategy?

*/

const sellLimitMinPriceAdjustment = 1.015;
const stopLossBuyLimitPriceAdjustment = 1.01;
const stopLossBuyBackGdaxPriceAdjustment = 1.03;
const stopLossBuyBackQuadrigaPriceAdjustment = 1.01;
const stopLossBuybackWaitPeriodMs = 1000 * 3;
const adjustOrderPriceQuotient = 0.001

const maxWaitingForVolumeSecs = 5;

const quadrigaDoubleTradeFee = 0.01
const quadrigaRateLimit = 2000;
const quadrigaMinOrderAmount = 0.0005;
const quadrigaMinOrderTotal = 1;

const consoleFgRed = "\x1b[31m";
const consoleFgMagenta = "\x1b[35m";
const consoleFgCyan = "\x1b[36m";
const consoleFgGreen = "\x1b[32m";
const consoleFgBlue = "\x1b[34m";
const consoleReset = "\x1b[0m";

export class Strategy {
	quadriga: IQuadrigaService;
	gdaxTicker: EventEmitter;

	gdaxTicks: TickerMessage[] = [];
	volume1s: number;
	
	balance: QuadrigaBalance;
	buyOrder: QuadrigaOpenOrder;
	sellOrder: QuadrigaOpenOrder;
	orderBook: QuadrigaOrderbook;
	userTransactionsHighestId: number = 0;

	meanSellPrice: number = 0;
	meanSellPriceTotalCadOutstanding: number = 0;

	lastLoop = {
		balanceRefreshed: false,
		orderBookRefreshed: false,
		buyBackTriggered: false,
		buyOrderActed: false,
		sellOrderActed: false,
		ordersRefreshed: false,
		userTransactionsRefreshed: false
	}

	nonce: number = 0;

	constructor(gdaxTicker: EventEmitter, quadriga: IQuadrigaService) {
		this.gdaxTicker = gdaxTicker;
		this.quadriga = quadriga;

		this.gdaxTicks = [];
	}

	async run() {
		this.gdaxTicker.on("data", (msg: any) => {
			// filter out all the garbage
			if (msg.type != "level" || msg.productId != "ETH-USD") {
				return;
			}
			this.gdaxTicks.push(msg);
		})

		this.balance = await this.quadriga.balance();
		this.balance.cad_available -= 0.01 * 2.5;
		this.balance.eth_available -= quadrigaMinOrderAmount * 2.5;
		
		await new Promise(resolve => setTimeout(resolve, quadrigaRateLimit));
		await this.ensureGdaxReady();
		this.volume1s = this.volume1h(await this.quadriga.transactions()) / 60 / 60;
		await new Promise(resolve => setTimeout(resolve, quadrigaRateLimit));
		var transactions = await this.quadriga.userTransactions();
		if (transactions.length > 0) {
			this.userTransactionsHighestId = transactions[0].id;
		}
		await new Promise(resolve => setTimeout(resolve, quadrigaRateLimit));

		while(true) {
			let timeout = new Promise(resolve => {setTimeout(resolve, quadrigaRateLimit)});
			this.trimGdaxTicks();

			if (this.lastLoop.balanceRefreshed === false) {
				let oldBalance = this.balance;
				this.balance = await this.quadriga.balance();

				//this.updateMeanSellPrice(oldBalance, this.balance);
				if (oldBalance.eth_balance != this.balance.eth_balance || oldBalance.cad_balance != this.balance.cad_balance) {
					console.log(consoleFgGreen, `${new Date()} Balance Eth:${this.balance.eth_balance} Cad:${this.balance.cad_balance} Avail Eth:${this.balance.eth_available} Cad:${this.balance.cad_available} Mean Sell Price:${this.tryGetMeanSellPrice()}`, consoleReset)
				}

				this.lastLoop.balanceRefreshed = true;
			// } else if (this.lastLoop.ordersRefreshed === false) {
			// 	var orders = await this.quadriga.openOrders();

			// 	this.buyOrder = orders.find(o => o.type == 0);
			// 	this.sellOrder = orders.find(o => o.type == 1);
			// 	this.lastLoop.ordersRefreshed = true
			} else if (this.lastLoop.userTransactionsRefreshed === false) {
				var transactions = await this.quadriga.userTransactions();
				this.updateMeanSellPrice(transactions);
				this.lastLoop.userTransactionsRefreshed = true;
			} else if (this.lastLoop.orderBookRefreshed === false) {
				this.orderBook = await this.quadriga.orderBook();
				//console.log(consoleFgBlue, `${new Date()} OrderBook sell:`,this.orderBook.asks[0], `buy`, this.orderBook.bids[0], consoleReset)
				this.lastLoop.orderBookRefreshed = true;
			} else if (this.lastLoop.sellOrderActed === false) {

				let gdaxPrice = this.gdaxPrice()
				let sellMin = gdaxPrice * sellLimitMinPriceAdjustment;
				let sellMax = this.getSellMax(this.orderBook)
				let sellPrice = Math.max(sellMin, sellMax);

				if (this.balance.eth_available * sellPrice < quadrigaMinOrderTotal) {
					this.lastLoop.sellOrderActed = true;
					// no action taken, opportunity to do another action here
					timeout = Promise.resolve({});
				} else if (this.sellOrder == null) {
					let amount = this.balance.eth_balance;
					console.log(consoleFgMagenta, new Date().toISOString(), "Sell order: " + amount + " Ξth at $" + sellPrice + " totalling $" + amount * sellPrice, consoleReset)
					console.log(consoleFgMagenta, `---- Min:${sellMin} Max:${sellMax} Gdax:${gdaxPrice}`, consoleReset)
					this.sellOrder = await this.quadriga.sell(amount, sellPrice, ++this.nonce);
					this.balance.eth_available -= amount;
					this.balance.eth_reserved += amount;
					this.lastLoop.sellOrderActed = true;
				} else if (Math.abs(this.sellOrder.price - sellPrice) / sellPrice >= adjustOrderPriceQuotient || (this.balance.eth_available > quadrigaMinOrderAmount && this.balance.eth_available * sellPrice > quadrigaMinOrderTotal)) {
					await this.quadriga.cancel(this.sellOrder.id);
					this.sellOrder = null;
				}
				else {
					this.lastLoop.sellOrderActed = true;
					// no action taken, opportunity to do another action here
					timeout = Promise.resolve({});
				}


			} else if (this.lastLoop.buyOrderActed === false) {

				let gdaxPrice = this.gdaxPrice()
				let buyMin = this.getBuyMin(this.orderBook);
				let meanSellPrice = this.tryGetMeanSellPrice()

				let buyMax = meanSellPrice === 0 ? buyMin : meanSellPrice * (1 - quadrigaDoubleTradeFee);
				let stopLossBuyLimit = gdaxPrice < this.getBuyTopOfBook(this.orderBook)[0]/*price*/ * stopLossBuyLimitPriceAdjustment ? 0 : this.getBuyTopOfBook(this.orderBook)[0]/*price*/ + 0.01
				let buyPrice = Math.max(Math.min(buyMin, buyMax), stopLossBuyLimit);
				let stopLossBuyBackTriggered = this.getStopLossBuybackTriggered(this.orderBook);

				if (this.balance.cad_available * buyPrice < quadrigaMinOrderAmount ||  this.balance.cad_available < quadrigaMinOrderTotal) {
					this.lastLoop.buyOrderActed = true;
					// no action taken, opportunity to do another action here
					timeout = Promise.resolve({});
				} else if (this.buyOrder == null) {

					if (stopLossBuyBackTriggered === true) {
						
						let buyBackPrice = this.getStopLossBuybackPrice(this.orderBook);
						let amount = this.balance.cad_balance / buyBackPrice;
						console.log(consoleFgRed, new Date().toISOString(), "Stop Loss! Buying back " + amount * buyBackPrice +" cad at " + buyBackPrice + ". Estimated loss: " + (meanSellPrice - buyBackPrice) * amount * quadrigaDoubleTradeFee * 0.01 + "%", consoleReset);
						console.log(consoleFgRed, `---- Min:${buyMin} Max:${buyMax} stopLossLimitOrder:${stopLossBuyLimit} gdax:${gdaxPrice} meanSellPrice: ${meanSellPrice}`, consoleReset)
						this.buyOrder = await this.quadriga.buy(amount, buyBackPrice, ++this.nonce);
						this.lastLoop.buyOrderActed = true;
					}
					else {
						let amount = this.balance.cad_balance / buyPrice;
						console.log(consoleFgCyan, new Date().toISOString(), "Buy order:  " + amount + " Ξth at $" + buyPrice + " totalling $" + amount * buyPrice, consoleReset)
						console.log(consoleFgCyan, `---- Min:${buyMin} Max:${buyMax} stopLossLimitOrder:${stopLossBuyLimit} gdax:${gdaxPrice} meanSellPrice: ${meanSellPrice}`, consoleReset)
						this.buyOrder = await this.quadriga.buy(amount, buyPrice, ++this.nonce);
						this.balance.cad_available -= amount;
						this.balance.cad_reserved += amount;
						this.lastLoop.buyOrderActed = true;
					}
				} else if (Math.abs(this.buyOrder.price - buyPrice) / buyPrice >= adjustOrderPriceQuotient || (this.balance.cad_available > quadrigaMinOrderTotal && this.balance.cad_available / buyPrice > quadrigaMinOrderAmount)) {
					await this.quadriga.cancel(this.buyOrder.id);
					this.buyOrder = null;
				}
				else {
					this.lastLoop.buyOrderActed = true;
					// no action taken, opportunity to do another action here
					timeout = Promise.resolve({});
				}



			}
			else {
				
				this.lastLoop = {
					balanceRefreshed: false,
					orderBookRefreshed: false,
					buyBackTriggered: false,
					buyOrderActed: false,
					sellOrderActed: false,
					ordersRefreshed: false,
					userTransactionsRefreshed: false
				}
				// no action taken, opportunity to do another action here
				timeout = Promise.resolve({});
			}

			


			


			await timeout;
		}
	}

	// returns mean sell price or 0
	private tryGetMeanSellPrice() {
		var mean = this.meanSellPrice;
		return Math.max(Number.isNaN(mean) ? 0 : mean , this.sellOrder != null ? this.sellOrder.price : 0)
	}

	private updateMeanSellPrice(transactions: QuadrigaUserTransaction[]) {
		// from https://math.stackexchange.com/a/198503
		// the amount sold is used as a weight in the calculation
		// when eth is rebought the weight is reduced
		// When no cad is held the weight is reset to 0 (should be extremely close to zero at this point)
		// if(this.sellOrder == null) {
		// 	return;
		// }

		
		
		for (let i = 0; i < transactions.length; i++) {
			const transaction = transactions[i];
			if (transaction.id  <= this.userTransactionsHighestId) {
				this.userTransactionsHighestId = transactions[0].id;
				break;
			}

			// Only parse eth_cad trades
			if(!transaction.eth || !transaction.cad || transaction.type != 2) {
				continue;
			}

			var cadTraded = Math.abs(transaction.cad);
			var price = Math.abs(transaction.cad / transaction.eth);

			if (transaction.eth < 0) {
				// selling, aka removing from the average

				// removing single value:
				// average = ((average * numValues) - value) / (numValues - 1);
				
				// adding multiple values (or combining two averages):
				// average = ((average * numValues) - value * newValues) / (numValues - newValues)

				this.meanSellPrice = ((this.meanSellPrice * this.meanSellPriceTotalCadOutstanding) - price * cadTraded) / (this.meanSellPriceTotalCadOutstanding - cadTraded)
				this.meanSellPriceTotalCadOutstanding = this.meanSellPriceTotalCadOutstanding - cadTraded;
				
			} else {
				// selling aka adding to the average
				
				// adding single value:
				// average = average + ((value - average) / numValues);

				// adding multiple values (or combining two averages):
				// average = ((average * numValues) + value * newValues) / (numValues + newValues)

				this.meanSellPrice = ((this.meanSellPrice * this.meanSellPriceTotalCadOutstanding) + price * cadTraded) / (this.meanSellPriceTotalCadOutstanding + cadTraded)
				this.meanSellPriceTotalCadOutstanding = this.meanSellPriceTotalCadOutstanding + cadTraded;
			}

			if (Number.isNaN(this.meanSellPrice)) {
				this.meanSellPrice = 0;
				console.log("mean sell price is NaN, because all cad was sold, resetting to 0")
			}

			if (Number.isNaN(this.meanSellPriceTotalCadOutstanding)) {
				this.meanSellPriceTotalCadOutstanding = 0;
				console.log("mean sell price cad outstanding is NaN, because all cad was sold, resetting to 0")
			}

			
		}

		//this.meanSellPriceCadRecieved = this.meanSellPriceCadRecieved + cadDifference;
		//this.meanSellPriceEthSent = this.meanSellPriceEthSent + ethDifference
		// let justSoldPrice = cadDifference / this.sellOrder.price;

		// let newMean = (this.meanSellPriceCadRecieved * this.meanSellPrice + justSoldPrice) / totalCadReceived;

		
		//if (newBalance.cad_balance < quadrigaMinOrderTotal) {
		//	this.meanSellPriceCadRecieved = 0;
		//	this.meanSellPriceEthSent = 0;
		//}
		
	}

	private trimGdaxTicks() {
		let now = this.gdaxTicks[0].time.getTime()

		let trimming = false;

		for (let i = 1; i < this.gdaxTicks.length; i++) {
			let time = this.gdaxTicks[i].time.getTime();
			let diff = now - time;
			
			if (diff > stopLossBuybackWaitPeriodMs) {
				if (trimming === false) {
					// leave one entry thats > the time
					trimming = true;
				} else {
					this.gdaxTicks = this.gdaxTicks.splice(i, 1);
					// since we just shortened the array, shorten i as well
					i--;
				}
			}

		}
	}

	// helpers
	private getSellMax(orderBook: QuadrigaOrderbook) {

		let ignorableOrderQuantityInBook = this.volume1s * maxWaitingForVolumeSecs;
		let i = 0;
		for (i = 0; i < orderBook.asks.length; i++) {
			const element = orderBook.asks[i];
			ignorableOrderQuantityInBook -= element[1] // order amount

			if (ignorableOrderQuantityInBook < 0) {
				break;
			}
		}

		let orderToUnderbid = orderBook.asks[i];

		return orderToUnderbid[0]/*price*/ - 0.01
	}

	private getBuyMin(orderBook: QuadrigaOrderbook) {

		let ignorableOrderQuantityInBook = this.volume1s * maxWaitingForVolumeSecs;
		let i = 0;
		for (i = 0; i < orderBook.bids.length; i++) {
			const element = orderBook.bids[i];
			ignorableOrderQuantityInBook -= element[1]/*amount*/

			if (ignorableOrderQuantityInBook < 0) {
				break;
			}
		}

		let orderToUnderbid = orderBook.bids[i];

		return orderToUnderbid[0]/*price*/ + 0.01
	}

	private getBuyTopOfBook(orderBook: QuadrigaOrderbook) {
		let ignorableOrderQuantityInBook = this.volume1s * maxWaitingForVolumeSecs;
		let i = 0;
		for (i = 0; i < orderBook.bids.length; i++) {
			const element = orderBook.bids[i];
			ignorableOrderQuantityInBook -= element[1] /*amount*/

			if (ignorableOrderQuantityInBook < 0) {
				break;
			}
		}

		return orderBook.bids[i];
	}

	private getStopLossBuybackPrice(orderBook: QuadrigaOrderbook) {
		let balance = this.balance.cad_balance;
		let price = 0;
		for (let i = 0; i < orderBook.asks.length; i++) {
			const element = orderBook.asks[i];
			balance -= element[1]/*amount*/ * element[0]/*price*/
			if (balance <= 0) {
				return element[0]/*price*/
			}
		}
	}

	private getStopLossBuybackTriggered(orderBook: QuadrigaOrderbook) {
		let meanSellPrice = this.tryGetMeanSellPrice();
		if (meanSellPrice === 0) return false;
		let gdaxUnsafePrice = meanSellPrice
		let quadrigaUnsafePrice = meanSellPrice
		let gdaxIsHigh = meanSellPrice <= this.gdaxPrice() / stopLossBuyBackGdaxPriceAdjustment
		let quadrigaIsHigh = quadrigaUnsafePrice <= this.getBuyTopOfBook(orderBook)[0] / stopLossBuyBackQuadrigaPriceAdjustment /*price*/ //and quadriga buy price > our sell price * 1.01

		let gdaxStayedHigh = true;
		if (gdaxIsHigh) {

			// check if gdax has been high for a while
			let now = this.gdaxTicks[0].time.getTime()
			for (let i = 0; i < this.gdaxTicks.length; i++) {
				const element = this.gdaxTicks[i];

				if(now - element.time.getTime() > stopLossBuybackWaitPeriodMs) {
					break;
				}

				if (element.price.toNumber() < gdaxUnsafePrice) {
					gdaxStayedHigh = false;
				}
			}

			

		}


		if (gdaxIsHigh == true && gdaxStayedHigh == true && quadrigaIsHigh == false) {
			console.log(consoleFgRed, "Gdax price unreasonably higher than quadriga", consoleReset)
		}

		return gdaxStayedHigh && quadrigaIsHigh
	} 
	
	private volume1h(transactions1h: QuadrigaTransaction[]) {
		let volume = 0;
		for (let i = 0; i < transactions1h.length; i++) {
			const element = transactions1h[i];
			volume += element.amount
		}
		
		return volume;
	}
	
	private gdaxPrice() {
		return this.gdaxTicks[0].price.toNumber();
	}

	async ensureGdaxReady() {
		while(this.gdaxTicks.length < 2) {
			await new Promise(resolve => {setTimeout(resolve, 100)});
		}
	}

}