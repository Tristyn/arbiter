import * as GTT from 'gdax-trading-toolkit';
import { GDAXFeed } from "gdax-trading-toolkit/build/src/exchanges"; 
import { OrderbookMessage, TickerMessage } from 'gdax-trading-toolkit/build/src/core';


const products: string[] = ["ETH-USD"];

export class GdaxTicker {

	count = 0;
	types = [];

	constructor() {
		
	}

	async feed() {
		var logger = GTT.utils.ConsoleLoggerFactory();
		var feed = await GTT.Factories.GDAX.FeedFactory(logger, products);
		var ethUsdFilter = new GTT.Core.ProductFilter({logger: logger, productId: "ETH-USD"})
		feed.pipe(ethUsdFilter);
		
		return ethUsdFilter;
	}

}