import * as QuadrigaCX from "quadrigacx";
const apiDefaults = {
	book: "eth_cad"
}

const functions = {
	orderBook: "order_book",
	balance: "balance",
	userTransactions: "user_transactions",
	buy: "buy",
	sell: "sell",
	cancel: "cancel",
	openOrders: "open_orders",
	transactions: "transactions",
}

const authTypes = {
	public: "public",
	private: "private"
}

export interface IQuadrigaService {
	transactions(): Promise<QuadrigaTransaction[]>
	userTransactions(): Promise<QuadrigaUserTransaction[]>
	orderBook(): Promise<QuadrigaOrderbook>
	balance(): Promise<QuadrigaBalance>
	buy(amount: number, price: number, nonce: number): Promise<QuadrigaOpenOrder>
	sell(amount: number, price: number, nonce: number): Promise<QuadrigaOpenOrder>
	cancel(id: string): Promise<boolean>
	openOrders(): Promise<QuadrigaOpenOrder[]>

}

export class QuadrigaService implements IQuadrigaService {

	api: QuadrigaCX;
	//functions
	//api(method, params, cb)
	//public_request(path, params, cb)
	//private_request(path, params, cb)

	constructor() {
		this.api = new QuadrigaCX(process.env.QUADRIGA_USERID,
			process.env.QUADRIGA_KEY, process.env.QUADRIGA_SECRET);
	}

	private async _call(path, authType: string, params?) {
		var params = { ...apiDefaults, ...params };

		var ret = await new Promise((resolve, reject) => {
			if(authType == authTypes.private) {
				this.api.private_request(path, params, (err, order) => {
					if (err) { reject(err); }
					resolve(order);
				})
			} else {
				this.api.public_request(path, params, (err, order) => {
					if (err) { reject(err); }
					resolve(order);
				})
			}
		});
		this.stringsToFloats(ret);
		return ret;

	}

	private stringsToFloats(obj) {
		for (var property in obj) {
			if (obj.hasOwnProperty(property)) {
				if (typeof obj[property] == "object") {
					this.stringsToFloats(obj[property]);
				}
				else if (typeof obj[property] == "string"){
					var float = parseFloat(obj[property]);
					if (!isNaN(float)) {
						obj[property] = float;
					}
				}
			}
		}
	}

	transactions() {
		return <Promise<QuadrigaTransaction[]>> this._call(functions.transactions, authTypes.public);
	}

	orderBook(): Promise<QuadrigaOrderbook> {
		return <Promise<QuadrigaOrderbook>> this._call(functions.orderBook, authTypes.public, {group: 0});
		//book.asks.map(val => { return {amount: Number.parseFloat(<any>val.amount)})
	}
	
	balance(): Promise<QuadrigaBalance> {
		return <Promise<QuadrigaBalance>>this._call(functions.balance, authTypes.private);
	}

	buy(amount: number, price: number, nonce: number) {
		return <Promise<QuadrigaOpenOrder>>this._call(functions.buy, authTypes.private, {amount, price, nonce});
	}

	sell(amount: number, price: number, nonce: number) {
		return <Promise<QuadrigaOpenOrder>>this._call(functions.sell, authTypes.private, {amount, price, nonce});
	}

	async cancel(id: string) {
		var found = <boolean>await this._call(functions.cancel, authTypes.private, {id})
		console.assert(found);
		return found;
	}

	openOrders() {
		return <Promise<QuadrigaOpenOrder[]>>this._call(functions.openOrders, authTypes.private);
	}
	

	userTransactions() {
		return <Promise<QuadrigaUserTransaction[]>>this._call(functions.userTransactions, authTypes.private);
	}
}

export class QuadrigaOrderbook {
	timestamp: string;
	bids: number[][]; // each array entry is an array where index 0 is the price and index 1 is the amount of eth 
	asks: number[][];
}

export class QuadrigaOpenOrder {
	id: string;
	datetime: number;
	type: 0 | 1; // buy or sell
	price: number;
	amount: number;
	status?: 0 | 1; // active or partially filled
}

export class QuadrigaBalance {
	eth_balance: number;
	eth_reserved: number;
	eth_available: number;
	cad_balance: number;
	cad_reserved: number;
	cad_available: number;
	fees: {
		eth_cad: number;
	};
}

export class QuadrigaTransaction {
	amount: number;
	date: number; // time in unix epoch
	price: number;
	tid: number;
	side: "buy" | "sell";
}

export class QuadrigaUserTransaction {
	datetime: number;
	id: number;
	type: 0 | 1 | 2;
	//method - deposit or withdrawal method
	eth?: number; // eth amount
	cad?: number; // cad amount
	order_id: string;
	fee: number;
	rate: number; // ????????????
}