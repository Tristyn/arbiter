import * as GTT from "gdax-trading-toolkit";
import { GdaxTicker } from "./GdaxTicker";
import { UsdCadFXService } from "./UsdCadFXService";
import { EventEmitter } from "events";
import { QuadrigaService, IQuadrigaService } from "./QuadrigaService";
import { QuadrigaSimulatorService } from "./QuadrigaSimulatorService";
import { Strategy } from "./Strategy";

require('dotenv').config();

(async function() {	
	var gdaxUsdTicker = await new GdaxTicker().feed()

	var usdCad = UsdCadFXService(process.env.DUMMYUSDCAD == "true");

	var cadFxFilter = new GTT.Core.ExchangeRateFilter({
		fxService: usdCad,
		pair: {from: "USD", to: "CAD"},
		precision: 2
	});

	var gdaxCadFilter = gdaxUsdTicker.pipe(cadFxFilter)
	var quadriga: IQuadrigaService = new QuadrigaService();

	if (process.env.SIMULATOR === "true") {
		var sim = new QuadrigaSimulatorService(quadriga);
		await sim.initialize();
		quadriga = sim;
	}

	await new Strategy(gdaxCadFilter, quadriga).run();

})();