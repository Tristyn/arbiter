import { FXProvider, CurrencyPair, FXObject, EFXRateUnavailable, FXRateCalculator, FXService } from "gdax-trading-toolkit/build/src/FXService";
import { BigJS } from "gdax-trading-toolkit/build/src/lib/types";
import * as GTT from "gdax-trading-toolkit";
import SimpleRateCalculator from "gdax-trading-toolkit/build/src/FXService/calculators/SimpleRateCalculator";

// export declare abstract class FXProvider {
//     private logger;
//     private _pending;
//     constructor(config: FXProviderConfig);
//     readonly abstract name: string;
//     log(level: string, message: string, meta?: any): void;
//     fetchCurrentRate(pair: CurrencyPair): Promise<FXObject>;
//     abstract supportsPair(pair: CurrencyPair): Promise<boolean>;
//     /**
//      * Returns a promise for the current rate. IsSupported must be true, and is not checked here. The method returns a
//      * promise for the current network request, or generates a new one.
//      * @param pair
//      * @returns {Promise<FXObject>}
//      */
//     protected getPromiseForRate(pair: CurrencyPair): Promise<FXObject>;
//     /**
//      * Fetch the latest FX exchange rate from the service provider and return a promise for an FXObject.
//      * If the service is down, or the latest value is unavailable, reject the promise with an EFXRateUnavailable error
//      * @param pair
//      */
//     protected abstract downloadCurrentRate(pair: CurrencyPair): Promise<FXObject>;
// }


export class DummyFXRateCalculator extends FXRateCalculator {
	pairs: { pair: CurrencyPair, rate: BigJS, time: Date }[] = [];
	
	async calculateRatesFor(pairs: CurrencyPair[]): Promise<FXObject[]> {
		var ret = [];
		for (let i = 0; i < pairs.length; i++) {
			var element = pairs[i];
			ret[i] = this.getRate(element);
		}
		return Promise.resolve(ret);
	}



	private getRate(pair: CurrencyPair): FXObject {
		for (let i = 0; i < this.pairs.length; i++) {
			var entry = this.pairs[i];
			var p = entry.pair;
			if ((pair.from == p.from && pair.to == p.to) ||
				(pair.to == p.from && pair.from == p.to)) {
				return {
					from: p.from,
					to: p.to,
					time: entry.time,
					rate: entry.rate
					//change?: BigJS;
				}
			}
		}

		console.log(pair);
		throw new EFXRateUnavailable("Currency pair not supported", "dummyFXService");
	}


	addPair(pair: CurrencyPair, rate: BigJS) {
		this.pairs.push({pair, rate, time: new Date()});
	}
}